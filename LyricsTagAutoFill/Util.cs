﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace LyricsTagAutoFill
{
	class Util
	{

		public static string StripHTML(string input)
        {
            return Regex.Replace(input, "<.*?>", String.Empty);
        }

		public static string replaceStrUsingRegex(string regex, string input, string replacement)
		{
			var delRegex = new System.Text.RegularExpressions.Regex(regex);
			return delRegex.Replace(input, replacement);
		}
	}

	public static class DictionaryExtensions
	{
		public static void AddOrReplace<TKey, TValue>(
			this Dictionary<TKey, TValue> self,
			TKey key,
			TValue value
		)
		{
			if (self.ContainsKey(key))
			{
				self[key] = value;
			}
			else
			{
				self.Add(key, value);
			}
		}
	}
}
