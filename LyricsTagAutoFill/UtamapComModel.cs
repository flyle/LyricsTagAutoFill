﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using hap = HtmlAgilityPack;
using System.Threading.Tasks;

namespace LyricsTagAutoFill
{
	class UtamapComModel : LyricsDLModel
	{
        //"/html/body/table[3]/tr[1]/td/table/tr[2]/td/a"
        public UtamapComModel(
			string postingUrlForGenSearchResultPage = "http://www.utamap.com/searchkasi.php",
			FormType formType = FormType.get,
			string xpathForExtMusicInfoTableOnSearchResultPage =
            "/html/body/table[3]/tr[1]/td/table/tr[position() >= 2]",
            string xpathForExtDetailUrlOnTable =
            "td/a",
            string xpathForExtArtistNameOnTable =
            "td[2]",
            // table->tableの直下の入れ子の場合うまく取れない？
            // table[3] / ここがつなぎめ
            string xpathForExtLyricsPageUrlOnDatailPage =
			"//*[@id=\"HeaderNavi\"]/div[1]/table[3]/tbody/tr/td[1]/table[3]/tbody/tr/td/table/tbody/tr[4]/td/script",
			string formDataEncoding = "shift_jis",
			string firstReferer = "http://www.utamap.com/indexkasi.html"
			) :
			base(
				postingUrlForGenSearchResultPage,
				formType,
				xpathForExtMusicInfoTableOnSearchResultPage,
                xpathForExtDetailUrlOnTable,
                xpathForExtArtistNameOnTable,
                xpathForExtLyricsPageUrlOnDatailPage,
                formDataEncoding,
				firstReferer)
		{
			formData.Add("act", "search");
			formData.Add("pattern", "4"); //前方,後方,部分,完全
			formData.Add("search_by_keyword", "検&#160;&#160;&#160;索");
			formData.Add("searchname", "title"); //曲名	
			formData.Add("sortname", "1"); //曲名昇順,曲名降順,新着順
			wa.browserIncognitoChrome59();
		}

		public override void TuneSearchOptionChange(MatchOption mo)
		{
			switch (mo)
			{
				case MatchOption.perfect:
					formData.AddOrReplace("pattern", "4");
					break;
				case MatchOption.partial:
					formData.AddOrReplace("pattern", "3");
					break;
				case MatchOption.forward:
					formData.AddOrReplace("pattern", "1");
					break;
			}
		}

		public override void Title(string t) { if (t != null && t != "") title = t; }
		public override void Artist(string a) { if (a != null) artist = a; }

		internal override string genLyricsPageUrl(hap.HtmlNode node)
		{
			throw new NotImplementedException();
		}

		// ./showkasi.php?surl=Fnnnnn ->
		// js_smt.php?unum=Fnnnnn
		private string genLyricsPageUrl(string detailPath)
		{
			var splitedString = detailPath.Split('=');
			string lyricsId = splitedString[1];
			return concatRelativeUrlToFullUrl("js_smt.php?unum=" + lyricsId);
		}

		internal override string stripLyrics()
		{
			string str1 = Util.replaceStrUsingRegex(
				@".*MS Pゴシック';", lyricsPageDom, "");
			string str2 = Util.replaceStrUsingRegex(
				@"document\.write\(\s*'<style\s+type=text/css>.*", str1, "");
			string str3 = Util.replaceStrUsingRegex(
				@"context2\.fillText\('", str2, "");
			string str4 = Util.replaceStrUsingRegex(
				@"',\d+,\d+,\d+\);", str3, "\r\n");
			string str5 = Util.replaceStrUsingRegex(
				"\\\\'", str4, "'"); //TextBoxから取得したときは@がいる？形式が違うようだ？
			return str5;
		}

		/** ページ遷移
		 *  1. 検索結果ページ
		 *  2. 歌詞本体ページ
		 **/
		// エントリメソッド
		public override async Task<string> getLyricsFromTitle()
		{
			formData.AddOrReplace("searchname", "title");
			formData.AddOrReplace("word", title);
			wa.setReferer("http://www.utamap.com/indexkasi.html");
			// 検索結果
			await downloadSearchResultPage();
            hap.HtmlNodeCollection musicInfoTable = getMusicsInfoOnSearchResult();
            if (musicInfoTable == null)	return "";
			// 詳細ページURLを取得
            string relativeDetailUrl = extDetailUrlFromMusicsInfoSearchWithArtist(musicInfoTable,true);
			if (relativeDetailUrl == null || relativeDetailUrl == "") return "";
			// 詳細ページURLから歌詞本体ページURLを生成 -> DL
			string lyricsContentUrl = genLyricsPageUrl(relativeDetailUrl);
			wa.setReferer(concatRelativeUrlToFullUrl(relativeDetailUrl));
			return await downloadLyricsPageAndGetLyrics(lyricsContentUrl);
		}

	}
}
