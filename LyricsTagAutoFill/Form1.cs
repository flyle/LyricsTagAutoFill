﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LyricsTagAutoFill
{
	public partial class Form1 : Form
	{
		Random rnd = new System.Random();

		private string[] supportExtension = {
			".flac", ".m4a", ".mp3", ".mp4"
		};

		public Form1()
		{
			InitializeComponent();
		}

		private List<string> musicFileSearch(string rootDir, bool isIncludingSubDir)
		{
			try
			{
				if (isIncludingSubDir)
				{
					return Directory.GetFiles(rootDir, "*.*", SearchOption.AllDirectories)
						.Where(s => supportExtension.Contains(Path.GetExtension(s).ToLower())).ToList();
				}
				else
				{
					return Directory.GetFiles(rootDir, "*.*")
						.Where(s => supportExtension.Contains(Path.GetExtension(s).ToLower())).ToList();
				}
			}
			catch (System.Exception excpt)
			{
				Console.WriteLine(excpt.Message);
				return new List<string>();
			}
		}

		private TagLib.File getTagLibClass(string filePath)
		{
			try
			{
				var file = TagLib.File.Create(filePath);
				return file;
			}
			catch
			{
				return null;
			}
		}

		private async void startButton_Click(object sender, EventArgs e)
		{
			Console.WriteLine("startClicked");
			startButton.Enabled = false;
			startButton.Text = "処理中";

			// パスから音楽ファイルリスト取得
			var _musicFilePath = musicFileSearch(folderLocationTextBox.Text, includeSubDirCheck.Checked);
			List<TagLib.File> _musicFileTagLib = new List<TagLib.File>();
			foreach(var file in _musicFilePath)
			{
				_musicFileTagLib.Add(getTagLibClass(file));
			}

			LyricsDLModel dlModel = null;
			if      (utaMapRadio.Checked) dlModel = new UtamapComModel();
			else if (utaNetRadio.Checked) dlModel = new UtaNetComModel();

			foreach (var tagLib in _musicFileTagLib.Where(f => f != null).ToList())
			{
				dlModel.Artist(tagLib.Tag.FirstPerformer);
				dlModel.Title(tagLib.Tag.Title);
				var res = await dlModel.getLyricsFromTitle();
				await Task.Delay(8000 + rnd.Next(1200) + rnd.Next(1600));
				if (res == "") continue;
				tagLib.Tag.Lyrics = res;
				tagLib.Save();
			}

			startButton.Enabled = true;
			startButton.Text = "開始";
		}

		/// <summary>
		/// 曲名で検索する時用
		/// </summary>
		private async void searchButton_Click(object sender, EventArgs e)
		{
			Console.WriteLine("searchClicked");
			searchButton.Enabled = false;
			searchButton.Text = "処理中";

			LyricsDLModel dlModel = null;
			if      (utaMapRadio.Checked) dlModel = new UtamapComModel();
			else if (utaNetRadio.Checked) dlModel = new UtaNetComModel();

			dlModel.TuneSearchOptionChange(MatchOption.partial);
			dlModel.Title(musicSearchBox.Text);
			dlModel.Artist(performerBox.Text);
			var res = await dlModel.getLyricsFromTitle();
			resultBox.Text = res;

			searchButton.Enabled = true;
			searchButton.Text = "曲名検索";
		}

		private void folderPickButton_Click(object sender, EventArgs e)
		{
            FolderBrowserDialog fbd = new FolderBrowserDialog();
            fbd.Description = "音楽ファイルの入っているフォルダを選択";
            fbd.RootFolder = Environment.SpecialFolder.MyComputer;
            fbd.SelectedPath = @"C:\";

            if (fbd.ShowDialog(this) == DialogResult.OK)
            {
                folderLocationTextBox.Text = fbd.SelectedPath;
            }
		}

		private void resultBox_Enter(object sender, EventArgs e)
		{
			this.resultBox.SelectAll();
		}
	}
}
