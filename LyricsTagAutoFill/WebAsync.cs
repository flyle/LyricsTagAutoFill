﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace LyricsTagAutoFill
{
	public enum UA
	{
		CHROME59_201706,
		FIREFOX53_201706
	}

	static class UserAgent
	{
		public static string getUa(this UA ua)
		{
			string[] userAgent =
			{
					"Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.104 Safari/537.36",
					"Mozilla/5.0 (Windows NT 6.3; WOW64; rv:53.0) Gecko/20100101 Firefox/53.0"
			};
			return userAgent[(int)ua];
		}
	}

	/// <summary>
	/// 簡易的なHTTPアクセスを実現するユーティリティクラスです。
	/// <para>UA=IE11 Lang=jp  TimeOut=5sec</para>
	/// </summary>
	class WebAsync
    {
		Dictionary<string, string> reqHeaders = new Dictionary<string, string>();

		int timeOut;

		public string recentUrl;

		private string failOverEncoding = "utf-8";

		internal WebAsync(string ua, string lang, string accept, string acceptEnc, int timeOut)
        {
			reqHeaders.Add("User-Agent", ua);
			reqHeaders.Add("Accept-Language", lang);
			reqHeaders.Add("Accept", accept);
			reqHeaders.Add("Accept-Encoding", acceptEnc);
			this.timeOut = timeOut;
		}

		/// <summary>
		/// IEのデフォルト設定に近い奴。タイムアウト5秒
		/// </summary>
        internal WebAsync()
        {
			reqHeaders.Add("User-Agent", 
				"Mozilla/5.0 (Windows NT 6.3; WOW64; Trident/7.0; rv:11.0) like Gecko");
			reqHeaders.Add("Accept-Language", "ja-JP");
			reqHeaders.Add("Accept", "text/html, application/xhtml+xml, */*");
			reqHeaders.Add("Accept-Encoding", "gzip, deflate");
            timeOut = 5;
		}

		public void browserIncognitoChrome59()
		{
			reqHeaders.AddOrReplace("User-Agent", UserAgent.getUa(UA.CHROME59_201706));
			reqHeaders.AddOrReplace("Accept-Language", "ja,en-US;q=0.8,en;q=0.6");
			reqHeaders.AddOrReplace("Accept-Encoding", "gzip, deflate");
			reqHeaders.AddOrReplace("Accept",
				"text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8");
		}

		public void setReferer(string refererUrl)
		{
			reqHeaders.AddOrReplace("Referer", refererUrl);
		}

		public void unsetReferer()
		{
			reqHeaders.Remove("Referer");
		}

		/// <summary>
		/// 指定URLのページをダウンロードします。
		/// </summary>
		/// <param name="url">対象となるURL</param>
		/// <returns>Webコンテンツ</returns>
		internal async Task<string> getAsync(string url)
        {
			recentUrl = url;
            using (var client = createHttpClientInstance())
            {
                using (var response = await client.GetAsync(url).ConfigureAwait(false))
                {
					try
					{
						return await response.Content.ReadAsStringAsync().ConfigureAwait(false);
					}
					catch
					{
						byte[] res = await response.Content.ReadAsByteArrayAsync().ConfigureAwait(false);
						return Encoding.GetEncoding(failOverEncoding).GetString(res, 0, res.Length);
					}
                }
            }
        }

		/// <summary>
		/// 指定URLに1項目で構成されるフォームデータをPOSTする
		/// </summary>
		/// <param name="url">POST先URL</param>
		/// <param name="name">input name</param>
		/// <param name="value">input value</param>
		/// <returns>Webコンテンツ</returns>
		internal async Task<string> postAsyncSingleElmForm(string url, string name, string value)
        {
			recentUrl = url;
            using (var client = createHttpClientInstance())
            {
                var postData = new FormUrlEncodedContent(new Dictionary<string, string>
                {
                    {name,value},
                });
                var response = await client.PostAsync(url, postData).ConfigureAwait(false);
                return await response.Content.ReadAsStringAsync().ConfigureAwait(false);
            }
        }

		/// <summary>
		/// 指定URLにフォームデータをPOSTする
		/// </summary>
		/// <param name="url">POST先URL</param>
		/// <param name="name">input name</param>
		/// <param name="value">input value</param>
		/// <returns>Webコンテンツ</returns>
		/// TODO 文字コードの変更未対応。C#のデフォルト文字コードで送信される。
		internal async Task<string> postAsyncForm(string postUrl, Dictionary<string,string> formDic)
		{
			recentUrl = postUrl;
			using (var client = createHttpClientInstance())
			{
				var formData = new FormUrlEncodedContent(formDic);
				var response = await client.PostAsync(postUrl, formData).ConfigureAwait(false);
				return await response.Content.ReadAsStringAsync().ConfigureAwait(false);
			}
		}

		/// <summary>
		/// 指定URLにフォームデータも用いてGETする
		/// </summary>
		/// <param name="url">POST先URL</param>
		/// <param name="name">input name</param>
		/// <param name="value">input value</param>
		/// <returns>Webコンテンツ</returns>
		/// 
		internal async Task<string> getAsyncForm(string getUrl, Dictionary<string, string> formDic, string enc)
		{
			string parameter = dicToHttpGetParameter(enc, formDic);
			return await getAsync(getUrl + parameter);
		
		}

		private HttpClient createHttpClientInstance()
		{
			var client = new HttpClient();

			foreach (KeyValuePair<string, string> kvp in reqHeaders)
			{
				client.DefaultRequestHeaders.Add(kvp.Key, kvp.Value);
			}

			client.Timeout = TimeSpan.FromSeconds(timeOut);

			return client;
		}

		private static string dicToHttpGetParameter(string encoding, Dictionary<string,string> parameterDic)
		{
			string parameter = "";

			foreach (KeyValuePair<string,string> kvp in parameterDic)
			{
				if( parameter == "")
				{
					parameter += "?";
				}
				else
				{
					parameter += "&";
				}

				byte[] data1 = Encoding.GetEncoding(encoding).GetBytes(kvp.Value);
				byte[] data2 = WebUtility.UrlEncodeToBytes(data1, 0, data1.Length);
				string encodedString = Encoding.GetEncoding(encoding).GetString(data2, 0, data2.Length);

				parameter += kvp.Key + "=" + encodedString;
			}

			return parameter;
		}
	}
}
