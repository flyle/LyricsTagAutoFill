﻿namespace LyricsTagAutoFill
{
	partial class Form1
	{
		/// <summary>
		/// 必要なデザイナー変数です。
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// 使用中のリソースをすべてクリーンアップします。
		/// </summary>
		/// <param name="disposing">マネージ リソースを破棄する場合は true を指定し、その他の場合は false を指定します。</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows フォーム デザイナーで生成されたコード

		/// <summary>
		/// デザイナー サポートに必要なメソッドです。このメソッドの内容を
		/// コード エディターで変更しないでください。
		/// </summary>
		private void InitializeComponent()
		{
			this.folderPickButton = new System.Windows.Forms.Button();
			this.startButton = new System.Windows.Forms.Button();
			this.musicSearchBox = new System.Windows.Forms.TextBox();
			this.resultBox = new System.Windows.Forms.TextBox();
			this.folderLocationTextBox = new System.Windows.Forms.TextBox();
			this.searchButton = new System.Windows.Forms.Button();
			this.label1 = new System.Windows.Forms.Label();
			this.performerBox = new System.Windows.Forms.TextBox();
			this.label2 = new System.Windows.Forms.Label();
			this.includeSubDirCheck = new System.Windows.Forms.CheckBox();
			this.utaMapRadio = new System.Windows.Forms.RadioButton();
			this.utaNetRadio = new System.Windows.Forms.RadioButton();
			this.searchSiteGroup = new System.Windows.Forms.GroupBox();
			this.dataGridView1 = new System.Windows.Forms.DataGridView();
			this.path = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.result = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.lyrics = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.lyricsSearchGroup = new System.Windows.Forms.GroupBox();
			this.lyricsFillGroup = new System.Windows.Forms.GroupBox();
			this.searchSiteGroup.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
			this.lyricsSearchGroup.SuspendLayout();
			this.lyricsFillGroup.SuspendLayout();
			this.SuspendLayout();
			// 
			// folderPickButton
			// 
			this.folderPickButton.Location = new System.Drawing.Point(388, 16);
			this.folderPickButton.Name = "folderPickButton";
			this.folderPickButton.Size = new System.Drawing.Size(75, 23);
			this.folderPickButton.TabIndex = 1;
			this.folderPickButton.Text = "参照";
			this.folderPickButton.UseVisualStyleBackColor = true;
			this.folderPickButton.Click += new System.EventHandler(this.folderPickButton_Click);
			// 
			// startButton
			// 
			this.startButton.Location = new System.Drawing.Point(388, 41);
			this.startButton.Name = "startButton";
			this.startButton.Size = new System.Drawing.Size(75, 23);
			this.startButton.TabIndex = 9;
			this.startButton.Text = "タグ付開始";
			this.startButton.UseVisualStyleBackColor = true;
			this.startButton.Click += new System.EventHandler(this.startButton_Click);
			// 
			// musicSearchBox
			// 
			this.musicSearchBox.Location = new System.Drawing.Point(64, 18);
			this.musicSearchBox.Name = "musicSearchBox";
			this.musicSearchBox.Size = new System.Drawing.Size(318, 19);
			this.musicSearchBox.TabIndex = 2;
			// 
			// resultBox
			// 
			this.resultBox.Location = new System.Drawing.Point(6, 68);
			this.resultBox.Multiline = true;
			this.resultBox.Name = "resultBox";
			this.resultBox.Size = new System.Drawing.Size(457, 142);
			this.resultBox.TabIndex = 5;
			// 
			// folderLocationTextBox
			// 
			this.folderLocationTextBox.Location = new System.Drawing.Point(6, 18);
			this.folderLocationTextBox.Name = "folderLocationTextBox";
			this.folderLocationTextBox.Size = new System.Drawing.Size(376, 19);
			this.folderLocationTextBox.TabIndex = 0;
			// 
			// searchButton
			// 
			this.searchButton.Location = new System.Drawing.Point(388, 18);
			this.searchButton.Name = "searchButton";
			this.searchButton.Size = new System.Drawing.Size(75, 44);
			this.searchButton.TabIndex = 4;
			this.searchButton.Text = "歌詞検索";
			this.searchButton.UseVisualStyleBackColor = true;
			this.searchButton.Click += new System.EventHandler(this.searchButton_Click);
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(18, 21);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(29, 12);
			this.label1.TabIndex = 10;
			this.label1.Text = "曲名";
			// 
			// performerBox
			// 
			this.performerBox.Location = new System.Drawing.Point(64, 43);
			this.performerBox.Name = "performerBox";
			this.performerBox.Size = new System.Drawing.Size(318, 19);
			this.performerBox.TabIndex = 3;
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(12, 46);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(41, 12);
			this.label2.TabIndex = 10;
			this.label2.Text = "歌手名";
			// 
			// includeSubDirCheck
			// 
			this.includeSubDirCheck.AutoSize = true;
			this.includeSubDirCheck.Checked = true;
			this.includeSubDirCheck.CheckState = System.Windows.Forms.CheckState.Checked;
			this.includeSubDirCheck.Location = new System.Drawing.Point(273, 46);
			this.includeSubDirCheck.Name = "includeSubDirCheck";
			this.includeSubDirCheck.Size = new System.Drawing.Size(109, 16);
			this.includeSubDirCheck.TabIndex = 11;
			this.includeSubDirCheck.Text = "サブフォルダも含む";
			this.includeSubDirCheck.UseVisualStyleBackColor = true;
			// 
			// utaMapRadio
			// 
			this.utaMapRadio.AutoSize = true;
			this.utaMapRadio.Location = new System.Drawing.Point(6, 18);
			this.utaMapRadio.Name = "utaMapRadio";
			this.utaMapRadio.Size = new System.Drawing.Size(66, 16);
			this.utaMapRadio.TabIndex = 12;
			this.utaMapRadio.Text = "うたまっぷ";
			this.utaMapRadio.UseVisualStyleBackColor = true;
			// 
			// utaNetRadio
			// 
			this.utaNetRadio.AutoSize = true;
			this.utaNetRadio.Checked = true;
			this.utaNetRadio.Location = new System.Drawing.Point(78, 18);
			this.utaNetRadio.Name = "utaNetRadio";
			this.utaNetRadio.Size = new System.Drawing.Size(63, 16);
			this.utaNetRadio.TabIndex = 13;
			this.utaNetRadio.TabStop = true;
			this.utaNetRadio.Text = "Uta-net";
			this.utaNetRadio.UseVisualStyleBackColor = true;
			// 
			// searchSiteGroup
			// 
			this.searchSiteGroup.Controls.Add(this.utaMapRadio);
			this.searchSiteGroup.Controls.Add(this.utaNetRadio);
			this.searchSiteGroup.Location = new System.Drawing.Point(13, 12);
			this.searchSiteGroup.Name = "searchSiteGroup";
			this.searchSiteGroup.Size = new System.Drawing.Size(469, 40);
			this.searchSiteGroup.TabIndex = 14;
			this.searchSiteGroup.TabStop = false;
			this.searchSiteGroup.Text = "歌詞検索サイト";
			// 
			// dataGridView1
			// 
			this.dataGridView1.AllowUserToAddRows = false;
			this.dataGridView1.AllowUserToDeleteRows = false;
			this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.path,
            this.result,
            this.lyrics});
			this.dataGridView1.Location = new System.Drawing.Point(12, 356);
			this.dataGridView1.Name = "dataGridView1";
			this.dataGridView1.ReadOnly = true;
			this.dataGridView1.RowHeadersVisible = false;
			this.dataGridView1.Size = new System.Drawing.Size(469, 149);
			this.dataGridView1.TabIndex = 15;
			// 
			// path
			// 
			this.path.HeaderText = "相対パス";
			this.path.Name = "path";
			this.path.ReadOnly = true;
			this.path.Width = 230;
			// 
			// result
			// 
			this.result.HeaderText = "成否";
			this.result.Name = "result";
			this.result.ReadOnly = true;
			this.result.Width = 35;
			// 
			// lyrics
			// 
			this.lyrics.HeaderText = "歌詞";
			this.lyrics.Name = "lyrics";
			this.lyrics.ReadOnly = true;
			this.lyrics.Width = 180;
			// 
			// lyricsSearchGroup
			// 
			this.lyricsSearchGroup.Controls.Add(this.resultBox);
			this.lyricsSearchGroup.Controls.Add(this.performerBox);
			this.lyricsSearchGroup.Controls.Add(this.musicSearchBox);
			this.lyricsSearchGroup.Controls.Add(this.searchButton);
			this.lyricsSearchGroup.Controls.Add(this.label2);
			this.lyricsSearchGroup.Controls.Add(this.label1);
			this.lyricsSearchGroup.Location = new System.Drawing.Point(12, 58);
			this.lyricsSearchGroup.Name = "lyricsSearchGroup";
			this.lyricsSearchGroup.Size = new System.Drawing.Size(469, 216);
			this.lyricsSearchGroup.TabIndex = 16;
			this.lyricsSearchGroup.TabStop = false;
			this.lyricsSearchGroup.Text = "歌詞情報の検索";
			// 
			// lyricsFillGroup
			// 
			this.lyricsFillGroup.Controls.Add(this.folderLocationTextBox);
			this.lyricsFillGroup.Controls.Add(this.folderPickButton);
			this.lyricsFillGroup.Controls.Add(this.includeSubDirCheck);
			this.lyricsFillGroup.Controls.Add(this.startButton);
			this.lyricsFillGroup.Location = new System.Drawing.Point(13, 280);
			this.lyricsFillGroup.Name = "lyricsFillGroup";
			this.lyricsFillGroup.Size = new System.Drawing.Size(469, 70);
			this.lyricsFillGroup.TabIndex = 17;
			this.lyricsFillGroup.TabStop = false;
			this.lyricsFillGroup.Text = "歌詞タグ付け";
			// 
			// Form1
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(494, 517);
			this.Controls.Add(this.lyricsSearchGroup);
			this.Controls.Add(this.dataGridView1);
			this.Controls.Add(this.searchSiteGroup);
			this.Controls.Add(this.lyricsFillGroup);
			this.Name = "Form1";
			this.Text = "歌詞タグ付与さん v1";
			this.searchSiteGroup.ResumeLayout(false);
			this.searchSiteGroup.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
			this.lyricsSearchGroup.ResumeLayout(false);
			this.lyricsSearchGroup.PerformLayout();
			this.lyricsFillGroup.ResumeLayout(false);
			this.lyricsFillGroup.PerformLayout();
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.Button folderPickButton;
		private System.Windows.Forms.Button startButton;
		private System.Windows.Forms.TextBox musicSearchBox;
		private System.Windows.Forms.TextBox resultBox;
        private System.Windows.Forms.TextBox folderLocationTextBox;
		private System.Windows.Forms.Button searchButton;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox performerBox;
        private System.Windows.Forms.Label label2;
		private System.Windows.Forms.CheckBox includeSubDirCheck;
		private System.Windows.Forms.RadioButton utaMapRadio;
		private System.Windows.Forms.RadioButton utaNetRadio;
		private System.Windows.Forms.GroupBox searchSiteGroup;
		private System.Windows.Forms.DataGridView dataGridView1;
		private System.Windows.Forms.DataGridViewTextBoxColumn path;
		private System.Windows.Forms.DataGridViewTextBoxColumn result;
		private System.Windows.Forms.DataGridViewTextBoxColumn lyrics;
		private System.Windows.Forms.GroupBox lyricsSearchGroup;
		private System.Windows.Forms.GroupBox lyricsFillGroup;
	}
}

