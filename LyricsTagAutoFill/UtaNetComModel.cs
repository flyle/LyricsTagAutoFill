﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using hap = HtmlAgilityPack;
using System.Threading.Tasks;
using CSharp.Japanese.Kanaxs;

namespace LyricsTagAutoFill
{
	class UtaNetComModel : LyricsDLModel
	{
		Random rnd;
		public UtaNetComModel(
			string postingUrlForGenSearchResultPage = "http://www.uta-net.com/search/",
			FormType formType = FormType.get,
			string xpathForExtMusicInfoTableOnSearchResultPage =
			"/html/body/div[2]/div[3]/table/tbody/tr",
			string xpathForExtDetailUrlOnTable =
			"td/a",
			string xpathForExtArtistNameOnTable =
			"td[2]/a",
			string xpathForExtLyricsPageUrlOnDatailPage =
			"//span[@id=\"ipad_kashi\"]/img",
			string formDataEncoding = "utf-8",
			string firstReferer = "http://www.uta-net.com/"
			) :
			base(
				postingUrlForGenSearchResultPage,
				formType,
				xpathForExtMusicInfoTableOnSearchResultPage,
				xpathForExtDetailUrlOnTable,
				xpathForExtArtistNameOnTable,
				xpathForExtLyricsPageUrlOnDatailPage,
				formDataEncoding,
				firstReferer
				)
		{
			rnd = new System.Random();
			formData.Add("Aselect", "2");
			// 1 歌手, 2 曲名, 3 作詞, 4 歌出し, 5 歌手平仮名, 6 曲平仮名, 7 番組、タイアップ, 8 作曲者, 10 歌詞の一部
			formData.Add("Bselect", "4"); // 1 前方, 2 後方, 3 含む, 4 完全一致
			wa.browserIncognitoChrome59();
		}

		public override void TuneSearchOptionChange(MatchOption mo)
		{
			switch (mo)
			{
				case MatchOption.perfect:
					formData.AddOrReplace("Bselect", "4");
					break;
				case MatchOption.partial:
					formData.AddOrReplace("Bselect", "3");
					break;
				case MatchOption.forward:
					formData.AddOrReplace("Bselect", "1");
					break;
			}
		}

		public override void Title(string t) {
			if (t != null && t != "") title = t;
			formDataXYsetter();
		}
		public override void Artist(string a) {
			if (a != null) artist = a;
			formDataXYsetter();
		}

		private void formDataXYsetter()
		{
			formData.AddOrReplace("x", (rnd.Next(98) + 1).ToString());
			formData.AddOrReplace("y", (rnd.Next(98) + 1).ToString());
		}

		internal override string genLyricsPageUrl(hap.HtmlNode node)
		{
			var detailPagePath = node.GetAttributeValue("src", "");
			return concatRelativeUrlToFullUrl(detailPagePath);
		}

		internal override string stripLyrics()
		{
			string str1 = Util.replaceStrUsingRegex(
				".*Meiryo,sans-serif\">", lyricsPageDom, "");
			string str2 = Util.replaceStrUsingRegex(
				"<text x=\"0\" y=\"\\d+\" font-size=\"15\">", str1, "");
			string str3 = Util.replaceStrUsingRegex(
				"(</text>)|(<text x=\"0\" y=\"\\d+\" font-size=\"15\"/>)", str2, "\r\n");
			string str4 = Util.replaceStrUsingRegex(
				@"<rect width=.*", str3, "");
			string str5 = Util.replaceStrUsingRegex(
				"　", str4, " "); //TextBoxから取得したときは@がいる？形式が違うようだ？
			return str5;
		}

		private string stripLyricsFromDetailPage()
		{
			hap.HtmlDocument hap = createHapDoc();
			hap.LoadHtml(detailPageDom);
			var node = hap.DocumentNode.SelectSingleNode("/html/body/div[3]/div[3]/div/div/div[3]/div");
			// /html/body/div[3]/div[3]/div/div/div[3]/div
			var nodeStr = node.InnerHtml;
			var str1 = Util.replaceStrUsingRegex(
				"<br>", nodeStr, "\r\n");
			var str2 = Util.replaceStrUsingRegex(
				"　", str1, " ");
			return str2;
		}

		/** ページ遷移
		 * 1. 検索結果ページ
		 * 2. 詳細ページ
		 * 3. (歌詞本体ページ)
		 **/
		// エントリメソッド
		public override async Task<string> getLyricsFromTitle()
		{
			formData.AddOrReplace("Keyword", title);
			wa.setReferer("http://www.uta-net.com/");
			//検索結果
			await downloadSearchResultPage();
			hap.HtmlNodeCollection musicInfoTable = getMusicsInfoOnSearchResult();
			if (musicInfoTable == null) return "";
			// 詳細ページ
			string relativeDetailUrl = extDetailUrlFromMusicsInfoSearchWithArtist(musicInfoTable,true);
			if (relativeDetailUrl == null || relativeDetailUrl == "") return "";
			string detailPageUrl = concatRelativeUrlToFullUrl(relativeDetailUrl);
			wa.setReferer(wa.recentUrl);
			string lyricsPageUrl = await downloadDetailPageAndGetLyricsPageUrl(detailPageUrl);
			wa.setReferer(wa.recentUrl);
			if (lyricsPageUrl != null)
			{
				return await downloadLyricsPageAndGetLyrics(lyricsPageUrl);
			}
			else
			{
				return stripLyricsFromDetailPage();
			}
		}

	}
}
