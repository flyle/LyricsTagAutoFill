﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using hap = HtmlAgilityPack;
using System.Text.RegularExpressions;
using CSharp.Japanese.Kanaxs;

namespace LyricsTagAutoFill
{
    public enum FormType
    {
        post, get
    }

	public enum MatchOption
	{
		perfect, partial, forward
	}

    public abstract class LyricsDLModel
    {
        private string postingUrlForGenSearchResultPage; //曲検索のaction要素
		private FormType ft; //検索で用いるHTTPリクエストの手段
		private string xpathForExtMusicInfoTableOnSearchResultPage; //検索結果から曲情報のtableタグ内
        private string xpathForExtDetailUrlOnTable; //曲情報tableタグから詳細URL
        private string xpathForExtArtistNameOnTable; //曲情報tableからアーティスト名
        private string xpathForExtLyricsPageUrlOnDatailPage; //詳細から歌詞本体URL
		private string formDataEncoding;
		private string searchResultPageDom;
		internal string detailPageDom;
		internal string lyricsPageDom;
		internal WebAsync wa;
		internal string title;
		internal string artist;

		protected Dictionary<string, string> formData;

        public LyricsDLModel(
            string postingUrlForGenSearchResultPage, FormType formType,
			string xpathForExtMusicInfoTableOnSearchResultPage,
            string xpathForExtDetailUrlOnTable,
            string xpathForExtArtistNameOnTable,
            string xpathForExtLyricsPageUrlOnDatailPage,
			string formDataEncoding,
			string firstReferer)
        {
			formData = new Dictionary<string, string>();
			this.postingUrlForGenSearchResultPage = postingUrlForGenSearchResultPage;
            this.ft = formType;
            this.xpathForExtMusicInfoTableOnSearchResultPage = xpathForExtMusicInfoTableOnSearchResultPage;
            this.xpathForExtDetailUrlOnTable = xpathForExtDetailUrlOnTable;
            this.xpathForExtArtistNameOnTable = xpathForExtArtistNameOnTable;
            this.xpathForExtLyricsPageUrlOnDatailPage = xpathForExtLyricsPageUrlOnDatailPage;
			this.formDataEncoding = formDataEncoding;
			wa = new WebAsync();
			wa.setReferer(firstReferer);
		}

		internal hap.HtmlDocument createHapDoc()
		{
			var hapDoc = new hap.HtmlDocument();
			hapDoc.OptionAutoCloseOnEnd = false;  //最後に自動で閉じる（？）
			hapDoc.OptionCheckSyntax = false;     //文法チェック。
			hapDoc.OptionFixNestedTags = true;    //閉じタグが欠如している場合の処理
			return hapDoc;
		}

		/// <summary>
		/// 曲名検索でのマッチオプション変更
		/// </summary>
		/// <param name="mo"></param>
		public abstract void TuneSearchOptionChange(MatchOption mo);

		/// <summary>
		/// 何らかの形で歌詞本体ページURLを生成するメソッド
		/// </summary>
		/// <returns></returns>
		internal abstract string genLyricsPageUrl(hap.HtmlNode node);

		/// <summary>
		/// 歌詞のみを置換処理して取り出し。歌詞本体ページから
		/// </summary>
		/// <returns></returns>
		internal abstract string stripLyrics();

		public virtual void Title(string t)  { if (t != null && t != "") title = t; }
		public virtual void Artist(string a) { if (a != null && a != "") artist = a; }

		/// <summary>
		/// エントリメソッド
		/// </summary>
		/// <returns></returns>
		public abstract Task<string> getLyricsFromTitle();

		private string erasePeriodAndSlash(string pathUri)
		{
			Regex removePeriodR = new Regex(@"^\.*/*");
			return removePeriodR.Replace(pathUri, "");
		}

		internal string concatRelativeUrlToFullUrl(string pathUrlBehindHost)
		{
			var uri = new Uri(postingUrlForGenSearchResultPage);
			var host = uri.Host;
			var scheme = uri.Scheme;
			return scheme + "://" + host + "/" + erasePeriodAndSlash(pathUrlBehindHost);
		}


        /// <summary>
        /// 検索結果から曲情報の抜き出し。null判定のみのため、不要なタグが含まれる可能性あり。
        /// サイトごとにその判定は実装すること
        /// </summary>
        /// <returns></returns>
        public hap.HtmlNodeCollection getMusicsInfoOnSearchResult()
        {
			var hapDoc = createHapDoc();
            hapDoc.LoadHtml(searchResultPageDom);
            var _nodes = hapDoc.DocumentNode.SelectNodes(xpathForExtMusicInfoTableOnSearchResultPage);

            if ( _nodes == null ) return null;
            return _nodes;
        }

		/// <summary>
		/// 1曲のデータが入ったnodeから詳細ページURL取得
		/// </summary>
		/// <returns></returns>
		private string getDetailPageUrlFromMusicInfo(hap.HtmlNode node)
		{
			var detailUrlTagNode = node.SelectSingleNode(xpathForExtDetailUrlOnTable);
			if (detailUrlTagNode == null) return null;
			var detailPagePath = detailUrlTagNode.GetAttributeValue("href", "");
			return detailPagePath;
		}

		/// <summary>
		/// 1曲のデータが入ったnodeからアーティスト名取得
		/// </summary>
		/// <returns></returns>
		private string getArtistNameFromMusicInfo(hap.HtmlNode node)
		{
			var artistNameTagNode = node.SelectSingleNode(xpathForExtArtistNameOnTable);
			if (artistNameTagNode == null) return null;
			var artistName = artistNameTagNode.InnerText;
			return artistName;
		}

		/// <summary>
		/// 詳細URLを取得。アーティスト名の合致しているもので調べる。
		/// アーティスト名での合致がない場合、最初の結果を返す
		/// </summary>
		/// <param name="musicsInfo"></param>
		/// <returns></returns>
		public string extDetailUrlFromMusicsInfoSearchWithArtist(hap.HtmlNodeCollection musicsInfo, bool hankakuCompare)
		{
			string filesArtist = this.artist;
			if (hankakuCompare) filesArtist = Kana.ToHankaku(this.artist);

			string detailUrl = getDetailPageUrlFromMusicInfo(musicsInfo.First()); //ひとまず最初の要素を許可
			foreach (var musicNode in musicsInfo)
			{
				string resultArtist = getArtistNameFromMusicInfo(musicNode);
				if (hankakuCompare) resultArtist = Kana.ToHankaku(resultArtist);

				if (resultArtist == filesArtist)
				{
					detailUrl = getDetailPageUrlFromMusicInfo(musicNode);
					return detailUrl;
				}
			}
			return detailUrl;
		}

		public async Task downloadDetailPage(string detailPageUrl)
		{
			detailPageDom = await wa.getAsync(detailPageUrl);
			return;
		}

		public async Task<string> downloadDetailPageAndGetLyricsPageUrl(string detailPageUrl)
		{
			await downloadDetailPage(detailPageUrl);
			var hapDoc = createHapDoc();
			hapDoc.LoadHtml(detailPageDom);
			var lyricsUrlNode = hapDoc.DocumentNode.SelectSingleNode(xpathForExtLyricsPageUrlOnDatailPage);
			if (lyricsUrlNode == null) return null;
			return genLyricsPageUrl(lyricsUrlNode);
		}

		public async Task downloadSearchResultPage()
		{
			switch (ft)
			{
				case FormType.get:
					searchResultPageDom = await wa.getAsyncForm(
					postingUrlForGenSearchResultPage, formData, formDataEncoding).ConfigureAwait(false);
					break;
				case FormType.post:
					searchResultPageDom = await wa.postAsyncForm(
					postingUrlForGenSearchResultPage, formData).ConfigureAwait(false);
					break;
			}
		}

		public async Task<string> downloadLyricsPageAndGetLyrics(string lyricsPageUrl)
		{
			lyricsPageDom = await wa.getAsync(lyricsPageUrl);
			return stripLyrics();
		}
        
    }
}
